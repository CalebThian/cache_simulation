verification using makefile:
make verify REQ_FILE=<trace.txt> VIC_FILE=<trace.out>

eg. make verify REQ_FILE=trace1.txt VIC_FILE=trace1.out

Note that if "permission denied" occurs, please enter the instruction below in terminal:
chmod +x Verify

command line format:
./cache <trace.txt> <trace.out>
eg. ./cache ./trace1.txt ./trace1.out
