#include <iostream>
#include <map>
#include <fstream>
#include <cmath>
#include <tgmath.h>
#include <vector>
#include <queue>
#include <utility>
#include <cstdlib>
#include <algorithm>
#include <iomanip>
#include <ctime>
using namespace std;

int max_size;
int policy;

bool cmp1(pair<int,int> a,pair <int,int> b){
	return a.first<b.first;
}

bool cmp2(pair<int,int> a,pair <int,int> b){
	return a.second>b.second;
}


class Catch_Block{
	private:
		int tag_num;
		vector < pair <int,int> > tag;	
		int pol;
	public:
		Catch_Block(){
			tag_num=0;
			pol=policy;
		};

		
		bool find(int inp,int l,int r){
			sort(tag.begin(),tag.end(),cmp1);
			while(l!=r){
				int middle=(r+l)/2;
				if(tag.at(middle).first==inp){
					return true;
				}else if(inp>tag.at(middle).first){
					if(l!=middle)
						l=middle;
					else 
						break;
				}else{
					r=middle;
				}
			}
			if(l!=tag.size()){
				if(tag.at(l).first==inp)
					return true;
				else
					return false;
			}else
				return false;
		};

		int index(int inp,int l,int r){
			sort(tag.begin(),tag.end(),cmp1);
			while(l!=r){
				int middle=(r+l)/2;
				if(tag.at(middle).first==inp){
					return middle;
				}else if(inp>tag.at(middle).first){
					if(l!=middle)
						l=middle;
					else
						break;
				}else{
					r=middle;
				}
			}
			if(l!=tag.size()){
				if(tag.at(l).first==inp)
					return l;
				else
					return -1;
			}else
				return -1;
			
		}

		int insert(int inp){
			if(index(inp,0,tag.size())!=-1){
				//cout<<"in"<<endl;
				if(pol==1){
				//cout<<"in"<<endl;
					int t=tag.at(index(inp,0,tag.size())).second;
					subtract(1,t);
					tag.at(index(inp,0,tag.size())).second=tag_num;
				}
				return -1;
			}
			else if(tag_num<max_size){
				if(!find(inp,0,tag.size())){
					++tag_num;
					tag.push_back(make_pair(inp,tag_num));
				}
				return -1;
			}else{
				sort(tag.begin(),tag.end(),cmp2);
				if(policy==2)
					shuffle_algo();
				if(tag.at(tag.size()-1).first!=inp){
					int temp=tag.at(tag.size()-1).first;
					tag.pop_back();
					subtract(1);
					tag.push_back(make_pair(inp,tag_num));
					return temp;	
				}else{
					return -1;
				}
			}
		};

		void subtract(int sub){
			for(int i=0;i<tag.size();++i)
				tag.at(i).second-=sub;
		}

		void subtract(int sub,int restraint){
			for(int i=0;i<tag.size();++i){
				if(tag.at(i).second>restraint)
					tag.at(i).second-=sub;
			}
		}

		void shuffle_algo(){
			for(int i=0;i<rand()%100;++i){
				int ind1=rand()%tag.size();
				int ind2=rand()%tag.size();
				pair <int,int> temp=tag.at(ind1);
				tag.at(ind1)=tag.at(ind2);
				tag.at(ind2)=temp;
			}
		}
		
		void print(){
			if(tag.size()!=0){
				for(int i=0;i<tag.size();++i)
					cout<<tag.at(i).first<<","<<tag.at(i).second<<"  ";
				cout<<endl;
			}
		}
};


int main(int argc,char* argv[]){
	srand(time(NULL));
	const unsigned w_memory=32;

	ifstream infile(argv[1],ios::in);
	ofstream outfile(argv[2],ios::out);	
	unsigned cache_size;
	unsigned block_size;
	int asc;//associativity;
	unsigned hex_a;
	infile>>cache_size;
	infile>>block_size;
	infile>>asc;
	infile>>policy;
	
	//cout<<cache_size<<endl;
	//cout<<block_size<<endl;
	//cout<<asc<<endl;
	//cout<<policy<<endl;

	//infile>>hex>>hex_a;cout<<hex_a<<endl;
	unsigned offset=log2(block_size);
	unsigned block_num=cache_size*1024/block_size;
	unsigned index;
	unsigned w_index;
	unsigned w_tag;
	if(asc==0){
		index=block_num;
		max_size=1;
	}
	else if(asc==1){
		index=block_num/4;
		max_size=4;
	}
	else if(asc==2){
		index=1;
		max_size=block_num;
	}

	w_index=log2(index);
	w_tag=w_memory-w_index-offset;

	//cout<<w_tag<<" "<<w_index<<" "<<offset<<endl;		
	Catch_Block cache[index];
	unsigned count=0;
	while(infile>>hex>>hex_a){
//		cout<<dec<<count<<" "<<hex<<hex_a<<endl;
		//cout<<(hex_a<<w_tag>>w_tag>>offset)<<endl;
		//cout<<(hex_a>>(w_index+offset))<<endl;
		//unsigned tag=hex_a>>(w_index+offset)<<(w_index+offset);
		outfile<<dec<<cache[hex_a<<w_tag>>w_tag>>offset].insert(hex_a>>(w_index+offset))<<endl;
		++count;
		/*for(int i=0;i<index;++i){
			cache[i].print();
		}*/
	}

	return 0;
}
